Dans les facultés, notamment SUP’COM en tant qu’ établissement universitaire dispose de plusieurs amphis et salles ce qui rend le processus 
de réservation d’une salle ou amphi ou encore des équipements bureautiques très sophistiqué surtout si ce processus est fait par une seule personne.
Donc pour répondre à ce besoin on a décidé de concevoir une application mobile qui va faciliter la procédure. La procédure de réservation existante 
se fait à travers l’administration, en fait, les clubs et les enseignants qui veulent réserver des salles ou des amphis pour les rattrapages ou les 
workshops doivent passer par les responsables administratifs qui vont prendre beaucoup du temps pour leurs fournir ce qu’ils désirent, car on doit 
toujours attendre la vérification de disponibilité des salles selon des calendriers bien saisis afin d’éviter tout chevauchement. 
parfois, vu les surcharges, les responsables oublient carrément de vérifier la disponibilité donc on risque d'envoyer plusieurs mails pour arriver 
finalement à réserver une salle. Pour éviter ce probléme on a trouvé la meilleure solution qui consiste à Développer une application mobile SUP'BOOKING 
qui permet au utilisateurs (étudiant/enseignant) de :
-Réserver des amphis , salles et des matériels . 
-Annuler la réservation si nécessaire 
-Consulter le planning des évenements.